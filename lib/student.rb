class Student
attr_accessor :first_name, :last_name, :courses

attr_reader :course, :students

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    first_name + " " + last_name
  end

  def enroll(new_course)

    raise "course enrolled" if courses.any? { |course| course.conflicts_with?(new_course) }

    return if courses.include?(new_course)

    courses << new_course

    new_course.students << self


  end

  def course_load

    course_loads = Hash.new(0)

    self.courses.each do |course|
      course_loads[course.department] += course.credits
    end

    course_loads

  end

end
